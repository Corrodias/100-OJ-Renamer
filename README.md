# 100% OJ Renamer
Since changing player names is a frequent request, you can use this instead of waiting for one of us to do it for you.

Download: https://drive.google.com/open?id=0By_du8NjegqCaGFZemFxVDRRa2c  
Image: http://imgur.com/Gn4iKNw

Instructions: **MAKE A BACKUP COPY OF YOUR SAVE FILE FIRST.** Load a file, choose a slot, edit it, and save. To cancel changing a name, just choose a different slot or exit the program.

Requires: .NET Framework 4.5

Source: https://gitlab.com/Corrodias/100-OJ-Renamer  
License: MIT w/ publicity clause