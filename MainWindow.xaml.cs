﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace _100_OJ_Renamer {

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        private string filename;
        private string name1;
        private string name2;
        private string name3;
        private string currentName;
        private int index;

        private static Cadenza.Collections.BidirectionalDictionary<byte, char> charMap
            = new Cadenza.Collections.BidirectionalDictionary<byte, char> {
                {0x0A, 'a'}, {0x0B, 'b'}, {0x0C, 'c'}, {0x0D, 'd'},
                {0x0E, 'e'}, {0x0F, 'f'}, {0x10, 'g'}, {0x11, 'h'},
                {0x12, 'i'}, {0x13, 'j'}, {0x14, 'k'}, {0x15, 'l'},
                {0x16, 'm'}, {0x17, 'n'}, {0x18, 'o'}, {0x19, 'p'},
                {0x1A, 'q'}, {0x1B, 'r'}, {0x1C, 's'}, {0x1D, 't'},
                {0x1E, 'u'}, {0x1F, 'v'}, {0x20, 'w'}, {0x21, 'x'},
                {0x22, 'y'}, {0x23, 'z'},
                {0x28, 'A'}, {0x29, 'B'}, {0x2A, 'C'}, {0x2B, 'D'},
                {0x2C, 'E'}, {0x2D, 'F'}, {0x2E, 'G'}, {0x2F, 'H'},
                {0x30, 'I'}, {0x31, 'J'}, {0x32, 'K'}, {0x33, 'L'},
                {0x34, 'M'}, {0x35, 'N'}, {0x36, 'O'}, {0x37, 'P'},
                {0x38, 'Q'}, {0x39, 'R'}, {0x3A, 'S'}, {0x3B, 'T'},
                {0x3C, 'U'}, {0x3D, 'V'}, {0x3E, 'W'}, {0x3F, 'X'},
                {0x40, 'Y'}, {0x41, 'Z'},
                {0x46, '0'}, {0x47, '1'}, {0x48, '2'}, {0x49, '3'},
                {0x4A, '4'}, {0x4B, '5'}, {0x4C, '6'}, {0x4D, '7'},
                {0x4E, '8'}, {0x4F, '9'},
                {0x50, ' '}, {0x51, '-'}, {0x52, '='}, {0x53, '/'},
                {0x54, ','}, {0x55, '.'}, {0x56, '\''}, {0x57, '&'},
                {0x58, '!'}, {0x59, '?'}, {0x5A, '#'}, {0x5B, '%'},
                {0x5C, '('}, {0x5D, ')'}, {0x5E, '♪'}, {0x5F, '★'},
            };

        public MainWindow() {
            InitializeComponent();
            setControlsEnabled(false);
        }

        private void setControlsEnabled(bool value) {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(contentGrid); i++) {
                // Retrieve child visual at specified index value.
                Visual childVisual = (Visual)VisualTreeHelper.GetChild(contentGrid, i);
                if (childVisual is Control && childVisual != loadButton)
                    ((Control)childVisual).IsEnabled = value;
            }
        }

        private void loadButton_Click(object sender, RoutedEventArgs e) {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "OJ Saves (*.dat)|*.dat|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == false)
                return;

            filename = openFileDialog.FileName;
            using (var fileStream = new FileStream(filename, FileMode.Open)) {
                var temp = new byte[10];
                fileStream.Seek(0x05, SeekOrigin.Begin);
                fileStream.Read(temp, 0, 10);
                name1 = arrayToName(temp);
                fileStream.Seek(0x61, SeekOrigin.Begin);
                fileStream.Read(temp, 0, 10);
                name2 = arrayToName(temp);
                fileStream.Seek(0xBD, SeekOrigin.Begin);
                fileStream.Read(temp, 0, 10);
                name3 = arrayToName(temp);
            }
            index = 0;
            slot1.IsChecked = true;
            setControlsEnabled(true);
        }

        private void okButton_Click(object sender, RoutedEventArgs e) {
            using (var fileStream = new FileStream(filename, FileMode.Open)) {
                try {
                    if (slot1.IsChecked ?? false) {
                        fileStream.Seek(0x05, SeekOrigin.Begin);
                        fileStream.Write(nameToArray(currentName), 0, 10);
                        name1 = currentName;
                    }
                } catch (InvalidDataException) { }
                try {
                    if (slot2.IsChecked ?? false) {
                        fileStream.Seek(0x61, SeekOrigin.Begin);
                        fileStream.Write(nameToArray(currentName), 0, 10);
                        name2 = currentName;
                    }
                } catch (InvalidDataException) { }
                try {
                    if (slot3.IsChecked ?? false) {
                        fileStream.Seek(0xBD, SeekOrigin.Begin);
                        fileStream.Write(nameToArray(currentName), 0, 10);
                        name3 = currentName;
                    }
                } catch (InvalidDataException) { }
            }
            MessageBox.Show("File saved!", "", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private string arrayToName(byte[] input) {
            var output = new StringBuilder(10);
            foreach (byte b in input) {
                if (!charMap.ContainsKey(b))
                    return "<invalid>";
                output.Append(charMap[b]);
            }
            return output.ToString().Trim();
        }

        private byte[] nameToArray(string input) {
            var output = Enumerable.Repeat<byte>(0x50, 10).ToArray();
            var paddedInput = input.PadRight(10, ' ');
            for (int i = 0; i < 10; i++) {
                var c = paddedInput[i];
                if (!charMap.Inverse.ContainsKey(c))
                    throw new InvalidDataException("Unrecognized character: " + c);
                output[i] = charMap.Inverse[c];
            }
            return output;
        }

        private void letter_Click(object sender, RoutedEventArgs e) {
            var button = ((Button)sender).Content.ToString();
            var letter = getButtonLetterText(button);
            var text = currentName.Insert(index++, letter);
            if (index > 9)
                index = 9;
            currentName = text.Substring(0, Math.Min(text.Length, 10));
            refreshNameBox();
        }

        private string getButtonLetterText(string button) {
            switch (button) {
                case "Button_&amp;": return "&";
                case "Button_note": return "♪";
                case "Button_star": return "★";
                default: return button.Substring(button.Length - 1);
            }
        }

        private void refreshNameBox() {
            name.Inlines.Clear();
            name.Inlines.Add(new Run(currentName.Substring(0, index)));
            if (index == currentName.Length) {
                name.Inlines.Add(new Underline(new Run('\u00A0'.ToString())));
            } else {
                var c = currentName.Substring(index, 1);
                name.Inlines.Add(new Underline(new Run(c)));
                name.Inlines.Add(new Run(currentName.Substring(index + 1, currentName.Length - (index + 1))));
            }
        }

        private void slot1_Checked(object sender, RoutedEventArgs e) {
            currentName = name1;
            index = Math.Min(currentName.Length, 10);
            refreshNameBox();
        }

        private void slot2_Checked(object sender, RoutedEventArgs e) {
            currentName = name2;
            index = Math.Min(currentName.Length, 10);
            refreshNameBox();
        }

        private void slot3_Checked(object sender, RoutedEventArgs e) {
            currentName = name3;
            index = Math.Min(currentName.Length, 10);
            refreshNameBox();
        }

        private void previousButton_Click(object sender, RoutedEventArgs e) {
            index--;
            if (index < 0)
                index = 0;
            refreshNameBox();
        }

        private void nextButton_Click(object sender, RoutedEventArgs e) {
            index++;
            if (index > 9)
                index = 9;
            if (index > currentName.Length)
                index = currentName.Length;
            refreshNameBox();
        }

        private void clearButton_Click(object sender, RoutedEventArgs e) {
            currentName = "";
            index = 0;
            refreshNameBox();
        }

        private void backButton_Click(object sender, RoutedEventArgs e) {
            if (index == 0)
                return;
            if (index < currentName.Length)
                currentName = currentName.Substring(0, index - 1) + currentName.Substring(index, currentName.Length - (index));
            if (index == currentName.Length)
                currentName = currentName.Substring(0, index - 1);
            index--;
            refreshNameBox();
        }
    }
}